@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Articulo: {{ $articulo->nombre}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::model($articulo,['method'=>'PATCH','route'=>['almacen.articulo.update',$articulo->idarticulo]])!!}
            {{Form::token()}}
            <div class="form-group">
            	<label for="nombre">Nombre</label>
            	<input type="text" name="nombre" class="form-control" value="{{$articulo->nombre}}" placeholder="Nombre...">
            </div>
            <div class="form-group">
            	<label for="codigo">Codigo</label>
            	<input type="text" name="codigo" class="form-control" value="{{$articulo->codigo}}" placeholder="Codigo...">
            </div>
			<div class="form-group">
			     <label>Categoria</label>
				   <select name="idcategoria" class="form-control">
						@foreach($categorias as $cat)
						  <option value="{{$cat->idcategoria}}">{{$cat->nombre}}</option>
						@endforeach
					</select>
			</div>
			<div class="form-group">
            	<label for="stock">Stock</label>
            	<input type="text" name="stock" class="form-control" value="{{$articulo->stock}}" placeholder="Stock...">
            </div>
			<div class="form-group">
            	<label for="estado">Estado</label>
            	<input type="text" name="estado" class="form-control" value="{{$articulo->estado}}" placeholder="Estado...">
            </div>

            <div class="form-group">
            	<button class="btn btn-primary" type="submit">Guardar</button>
            	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>

			{!!Form::close()!!}		
            
		</div>
	</div>
@endsection