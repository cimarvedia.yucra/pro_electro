@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Usuario: {{ $usuario->name}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::model($usuario,['method'=>'PATCH','route'=>['seguridad.usuario.update',$usuario->id]])!!}
            {{Form::token()}}
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
						<div class="row">

							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">   
									<div class="form-group">
										<label for="name">Nombre</label>
										<input id="name" type="text" name="name" required value="{{$usuario->name}}" class="form-control" placeholder="Nombre...">
										@if ($errors->has('name'))
											<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>

							<div class="form-group">
							    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								    <div class="form-group">
										<label for="email">Correo</label>
										<input id="email" type="text" name="email" required value="{{$usuario->email}}" class="form-control" placeholder="Correo...">
									   @if ($errors->has('email'))
											<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
								    </div>
								</div>
							</div>

							<div class="form-group ">
							    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							     <label for="password">Contraseña </label>
                                    <input id="password" name="password" class="form-control" placeholder="Password...">
									@if ($errors->has('password'))
										<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
											<label for="password-confirm">Confirmar Contraseña</label>
											<input id="password-confirm" type="password" name="password_confirmation"   class="form-control" placeholder="Password-confirm...">
										@if ($errors->has('password_confirmation'))
											<span class="help-block">
												<strong>{{ $errors->first('password_confirmation') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</div>
								

			       
					
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Guardar</button>
								<button class="btn btn-danger" type="reset">Cancelar</button>
							</div>
							</div>
					    </div>
					</form>

			{!!Form::close()!!}		
            
		</div>
	</div>
@endsection