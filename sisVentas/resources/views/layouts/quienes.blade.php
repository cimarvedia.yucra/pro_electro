<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ELECTRODOMESTICOS </title>
    <meta name="description" content="Demo of A Free Coming Soon Bootstrap 4 Template by TemplateFlip.com."/>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="styles/main.css" rel="stylesheet">
  </head>
  <body id="top" >
    
    <div class="site-wrapper">
 <div class="site-wrapper-inner">
    <div class="cover-container">

      
 <!-- PARA INCIO Y REGISTRO -->
 <div class="row">   
    <div class="col-6">

        <!-- BOTONES MENU -->
<div class="container" >
        <div class="row">
             <div class="col"></div>
            <div class="col">
                <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="index.html"><button type="button" class="btn btn-secondary" href="#index.html">Inicio</button></a>
                     <a href="quienes_somos"><button type="button" class="btn btn-secondary" >Quienes Somos</button></a>
                     <a href="nuestrop">  <button type="button" class="btn btn-secondary">Nuestros Productos</button></a>
                </div>
    
            </div>
            <div class="col"></div>
        </div>    
      </div>
    </div>
    <div class="col-6">
            <button type="button" class="btn btn-secondary"  data-toggle="modal" data-target="#reg">Registrarse             
                </button>
                <div class="modal" tabindex="-1" role="dialog" id="reg">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">Registrate</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                    <form>
                                            <div class="form-group">
                                                <label for="nombre">Nombre</label>
                                                <input type="text" class="form-control" id="nombre">
                                            </div>
                                            <div class="form-group">
                                                    <label for="app">Apellidos</label>
                                                    <input type="text" class="form-control" id="app">
                                                </div>  
                                                <div class="form-group">
                                              <label for="exampleInputEmail1">Correo</label>
                                              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="correo">
                                              <small id="emailHelp" class="form-text text-muted">Nunca compartiremos su email con nadie</small>
                                            </div>
                                            <div class="form-group">
                                              <label for="exampleInputPassword1">Contraseña</label>
                                              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                                            </div>
                                         <div class="form-group">
                                                        <label for="fechanac">Fecha de Nacimiento</label>
                                                        <input type="date" class="form-control" id="fechanac">
                                                    </div>

                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-primary">Registrarse</button>
                                     </form>
                            </div>

                          </div>
                        </div>
                      </div>
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#inicio">Iniciar Sesion</button>
            <div class="modal" tabindex="-1" role="dialog" id="inicio">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Iniciar Sesion</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Correo Electronico</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">No compartiremos su mail con nadie.</small>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                          </div>
                          <button type="submit" class="btn btn-primary">Inicio</button>
                        </form>
                      </div>
    </div>
  </div>
</div>
</div>
</div>
        <!-- IMAGENES CARRUSEL-->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="imag/portada1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="imag/portada2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="imag/portada3.png" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>
      
        <p class="lead cover-copy">Porque tu hogar estara bien equipado con nosotros.</p>     

           <!-- QUIENES SOMOS -->
           <div class="row">
             <div class="col-1"></div>
                  <div class="col-9" style="text-align:justify">
         <H1><b>QUIENES SOMOS</b></H1> 
         <p> Somos una empresa la cual piensa en su tiempo y es por eso que 
             Usted con nosotros puede comprar via Online o pasar por nuestras oficinas
          Llevamos nuestros productos a su domicilio sin ningun recargo.
          <br>
          <br> Contamos con una amplia variedad de productos la cual
           puede elegir lo que necesita para su hogar.Nuestro sitio es el primero a nivel nacional.Somos una tienda en línea donde usted puede comprar todos sus productos electrónicos
           y electrodomesticos,  Puede hacer que se los entreguen directamente a usted. Compre en línea con gran facilidad ya que paga con TigoMoney, que le garantiza la compra en línea más segura, método de pago, lo que le permite realizar pagos sin estrés. 
            <br> Sea lo que sea que desee comprar, Nuestra empresa le ofrece a todos y mucho más a precios en los que puede confiar.
         </p>
</div>
           </div>
     

</div>























<div class="row" class="container">
 
        <div class="col-9" style="text-align:right">
        <div class="fixed-bottom"><button type="button" class="btn btn-info">Contactanos</button></div>
   </div>
</div>

<div class="row">
    <div class="col-1"></div>
    <div class="col">
        <div>
        <small class="text-muted">Electrodomesticos Derechos Reservados 2018</small>
      </div>
    </div>
   
</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>