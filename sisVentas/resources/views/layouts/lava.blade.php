<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ELECTRODOMESTICOS</title>
    <meta name="description" content="Demo of A Free Coming Soon Bootstrap 4 Template by TemplateFlip.com."/>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="styles/main.css" rel="stylesheet">
  </head>
  <body id="top" >
    
    <div class="site-wrapper">
 <div class="site-wrapper-inner">
    <div class="cover-container">

      
 <!-- PARA INCIO Y REGISTRO -->
  <div class="row">   
    <div class="col">

        <!-- BOTONES MENU -->
<div  >
        <div class="row">
    
            <div class="col-2">
                <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="index.html"><button type="button" class="btn btn-secondary" href="#index.html">Inicio</button></a>
                     <a href="quienes_somos"><button type="button" class="btn btn-secondary" >Quienes Somos</button></a>
                     <a href="nuestrop">  <button type="button" class="btn btn-secondary">Nuestros Productos</button></a>
                </div>
    
            </div>
   
        </div>    
      </div>
    </div>
    <div class="col-3">
                      
             
      
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#inicio">Iniciar Sesion</button>
            <div class="modal" tabindex="-1" role="dialog" id="inicio">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Iniciar Sesion</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Correo Electronico</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">No compartiremos su mail con nadie.</small>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                          </div>
                          <button type="submit" class="btn btn-primary">Inicio</button>
                        </form>
                      </div>
    </div>
  </div>
</div>
</div>
</div>
        <!-- IMAGENES CARRUSEL-->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="imag/portada1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="imag/portada2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="imag/portada3.png" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>
      
        <p class="lead cover-copy">Porque tu hogar estara bien equipado con nosotros.</p>     

  <div class="row" class="container"> 
        <div class="col-2">
    
             
                     
                      <div class="card-body">
                          <a href="volver" class="href">  <div class="row"><button type="button" class="btn dark mb-3" >Volver</button></div> </a> 
                              
                      </div>
        </div>
      
      </div>
 
             <!-- IMAGENES DE LAS CATEGORIAS-->


          <div class="row">

          <div class="col">

          
             <div class="card-group">
                <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                    <img class="card-img-top" src="imag/lavadoras/lava2.jpg" alt="Card image cap" href="lava.html" >
                  <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                    <h5 class="card-title">LAVADORA 8kl</h5>
                    <p class="card-text">Lavadora Phillips 8 kls.</p>
                  </div>
 
                </div>
                <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                  <img class="card-img-top" src="imag/lavadoras/lava3.jpg" alt="Card image cap">
                  <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                    <h5 class="card-title">LAVADORA LG</h5>
                    <p class="card-text">Lavadora LG 15 kls</p>
                  </div>
            
                </div>
                <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                  <img class="card-img-top" src="imag/lavadoras/lava4.jpg" alt="Card image cap">
                  <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                    <h5 class="card-title">LAVADORA SAMSUNG</h5>
                    <p class="card-text">Lavadora LG 12 kls.</p>
                  </div>
                  
             
                </div>
              </div>
            </div>
          </div>
          <div class="row">

              <div class="col">
    
              
                 <div class="card-group">
                    <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                      <img class="card-img-top" src="imag/lavadoras/lava5.jpg" alt="Card image cap">
                      <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                        <h5 class="card-title">LAVADORA</h5>
                        <p class="card-text">Lavadora LG 12 kls</p>
                      </div>
                     
                    </div>
                    <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                      <img class="card-img-top" src="imag/lavadoras/lava6.jpg" alt="Card image cap">
                      <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                        <h5 class="card-title">LAVADORA</h5>
                        <p class="card-text">Lavadora LG 16 kls </p>
                      </div>
                      
                    </div>
                    <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                      <img class="card-img-top" src="imag/lavadoras/lava7.jpg" alt="Card image cap">
                      <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                        <h5 class="card-title">LAVADORA </h5>
                        <p class="card-text">Lavadora LG 20 kls</p>
                      </div>
                      
                     
                    </div>
                  </div>
                </div>
              </div>


































          <div class="row" class="container">
 
                <div class="col-9" style="text-align:right">
                <div class="fixed-bottom"><button type="button" class="btn btn-info">Contactanos</button></div>
           </div>
       </div>
   
       <div class="row">
          <div class="col-1"></div>
          <div class="col">
              <div>
              <small class="text-muted">Electrodomesticos Derechos Reservados 2018</small>
            </div>
          </div>
         
      </div>
               
       <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
       <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
   </body>
   </html>