<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ELECTRODOMESTICOS</title>
    <meta name="description" content="Demo of A Free Coming Soon Bootstrap 4 Template by TemplateFlip.com."/>
    <link href="{{asset('https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700')}}" rel="stylesheet">
    <link href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('styles/main.css')}}" rel="stylesheet">
  </head>
  <body id="top" >
    
    <div class="site-wrapper">
 <div class="site-wrapper-inner">
    <div class="cover-container">

      
 <!-- PARA INCIO Y REGISTRO -->
 <div class="row">   
    <div class="col">

        <!-- BOTONES MENU -->
<div  >
        <div class="row">
    
            <div class="col-2">
                <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="index.html"><button type="button" class="btn btn-secondary" href="#index.html">Inicio</button></a>
                     <a href="quienes_somos"><button type="button" class="btn btn-secondary" >Quienes Somos</button></a>
                     <a href="nuestrop">  <button type="button" class="btn btn-secondary">Nuestros Productos</button></a>
                </div>
    
            </div>
   
        </div>    
      </div>
    </div>
    <div class="col-3">

    
           

         
          <li><a href="{{url('layouts/app')}}"><button type="button" class="btn btn-secondary" >Iniciar Sesión</button></a></li>
         
          <!-- VENTANA DE INCIO DE SESION-->
            <!-- <div class="modal" tabindex="-1" role="dialog" id="inicio">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Iniciar Sesion</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Correo Electronico</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">No compartiremos su mail con nadie.</small>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña">
                          </div>
                          <button type="submit" class="btn btn-primary">Inicio</button>
                        </form>
                      </div>
               </div>
             </div>
            </div>
          -->
</div>
</div>

        <!-- IMAGENES CARRUSEL-->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="imag/portada1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="imag/portada2.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="imag/portada3.png" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>
       <p class="lead cover-copy">Porque tu hogar estara bien equipado con nosotros.</p> 
           

  
   <!-- MENU PRINCIPAL -->
   <!--
    
      <div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="subscribeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="subscribeModalLabel">Get Notified on Launch:</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Enter you e-mail to get notified when we launch</label>
                  <input type="text" class="form-control" id="recipient-name" placeholder="your-name@example.com">
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default">Subscribe</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>



-->
<div class="row" class="container"> 
    <div class="col-2">

          <div class="card text-dark" style="background-color: rgba(198, 209, 198, 0.322)">
                  <div class="card-header text-white">Categorias</div>
                  <div class="card-body">
                         <li><a href="lavadora"> <div class="row"><button type="button" class="btn dark mb-3" >Lavadoras</button></div></a> 

                        <a href="seca"> <div class="row"><button type="button" class="btn dark mb-3">Secadoras</button></div></a> 

                          <a href="televisores">  <div class="row"><button type="button" class="btn dark mb-3">Televisores</button></div></a>
                           
                       <a href="cosinas">  <div class="row"><button type="button" class="btn dark mb-3">Cocinas</button></div> </a> 

                        <a href="heladera">  <div class="row"><button type="button" class="btn dark mb-3">Heladeras</button></div>  </a> 

                          <a href="micros">  <div class="row"><button type="button" class="btn dark mb-3">Microondas</button></div>  </a> 
                  </div>
    </div>
  
  </div>

    <div class="col-10"  >
        <div class="card-header"> <b> Productos Destacados</b></div>

        <div class="card-group"  style="background-color: rgba(198, 209, 198, 0.322)">
                <div class="card"   style="background-color: rgba(198, 209, 198, 0.322)">
                  <img class="card-img-top" src="imag/tv/uhd.jpg" alt="Card image cap" >
                  <div class="card-body text-white" >
                    <h5 class="card-title text-white"  >Samsung 75 '' UHD 4K SMART TV-75MU6300F</h5>
                    <p class="card-text text-white" >Características principales
                            UHD 4K TV
                            RESOLUCIÓN 3840 * 2160
                            3 HDMI
                            2 USB
                            AV / COMPONENTE
                            LAN</p>
                    <p class="card-text"><small class="text-muted">650$</small></p>
                  </div>
                </div>
             
              
              <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                  <img class="card-img-top" src="imag/tv/lg1.jpg" alt="Card image cap">
                  <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                    <h5 class="card-title">LG 60LJ500 60-Inch Smart 4K UHD TV - Web Os</h5>
                    <p class="card-text">CARACTERÍSTICAS PRINCIPALES
                            Tamaño de pantalla: 60 pulgadas
                            Resolución: (3840 x 2160) Ultra HD
                            Índice de reproducción de imágenes: 700
                            Identificar automáticamente los estándares de audio DTS Neck
                            Sistema de altavoces / altavoz 2ch / 20W
                            Sistema operativo: webOS 2.0</p>
                    <p class="card-text"><small class="text-muted">650$</small></p>
                  </div>
                </div>
                <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                    <img class="card-img-top" src="imag/tv/h1.jpg" alt="Card image cap">
                    <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                      <h5 class="card-title">Hisense 55''Smart UHD 4K TV</h5>
                      <p class="card-text"> 
                              Características principales
                              12 meses de garantía
                              Pantalla de 50 pulgadas
                              Resolución 3840 * 2160
                              4K Upscaling, Decodificación, Streaming
                              Televisión satelital
                              WIFI incorporado</p>
                      <p class="card-text"><small class="text-muted">500$</small></p>
                    </div>
                  </div>
             

            </div>

            
          </div>
          <div class="col-2"></div>
          <div class="col-10">
              <div class="card-header"></div>
              <div class="card-group" style="background-color: rgba(198, 209, 198, 0.322)">
                      <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                        <img class="card-img-top" src="imag/lavadoras/c1.jpg" alt="Card image cap">
                        <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                          <h5 class="card-title">Bosch Serie 6</h5>
                          <p class="card-text">Bosch Serie 6 WUQ24468ES Lavadora de Carga Frontal 8Kg A+++ Blanca opiniones.</p>
                          <p class="card-text"><small class="text-muted">200$</small></p>
                        </div>
                      </div>
                      <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                        <img class="card-img-top" src="imag/lavadoras/dw1.jpg" alt="Card image cap">
                        <div class="card-body"style="background-color: rgba(198, 209, 198, 0.322)">
                          <h5 class="card-title">DAEWOO 10 kls.</h5>
                          <p class="card-text">Lavadora automática de 10 kilos silver DAEWOO.</p>
                          <p class="card-text"><small class="text-muted">250$</small></p>
                        </div>
                      </div>
                      <div class="card" style="background-color: rgba(198, 209, 198, 0.322)">
                        <img class="card-img-top" src="imag/lavadoras/w1.jpg" alt="Card image cap">
                        <div class="card-body" style="background-color: rgba(198, 209, 198, 0.322)">
                          <h5 class="card-title">LG TWINWash™</h5>
                          <p class="card-text"> Lavadora Set Carga Frontal y Twinwash 25,5 Kg – TWW22VVS2-PKG LG.</p>
                          <p class="card-text"><small class="text-muted">500$</small></p>
                        </div>
                      </div>
                    </div>  

        </div>





        </div>
    </div>  



<br>
<br>
<br>
<div class="row">
    <div class="col-1"></div>
    <div class="col">
        <div>
        <small class="text-muted">Electrodomesticos Derechos Reservados 2018</small>
      </div>
    </div>
   
</div>
    <div class="row" class="container">

         <div class="col-9" style="text-align:right">
         <div class="fixed-bottom"><button type="button" class="btn btn-info">Contactanos</button></div>
    </div>
</div>

<br>



</div>



  

    <script src="{{asset('https://code.jquery.com/jquery-3.2.1.slim.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js')}}"></script>
    <script src="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('scripts/main.js"></script>
  </body>
</html>