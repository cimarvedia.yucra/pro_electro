<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('layouts/index');
});
//RUTAS CATEGORIAS DE PRODUCTOS 
Route::get('/lavadora', function () {
    return view('layouts/lava');
});
Route::get('/seca', function () {
    return view('layouts/secadora');
});
Route::get('/cosinas', function () {
    return view('layouts/cocina');
});
Route::get('/televisores', function () {
    return view('layouts/tv');
});
Route::get('/heladera', function () {
    return view('layouts/hela');
});
Route::get('/micros', function () {
    return view('layouts/micro');
});
//MENUN DE PRINCIPAL
Route::get('/quienes_somos', function () {
    return view('layouts/quienes');
});
Route::get('/nuestrop', function () {
    return view('layouts/nuestro');
});
Route::get('/index.html', function () {
    return view('layouts/index');
});
//BOTON DE VOLVER A LA PAGINA DE INICIO
Route::get('/volver', function () {
    return view('layouts/index');
});



Route::resource('almacen/categoria','CategoriaController');
Route::resource('almacen/articulo','ArticuloController');
Route::resource('seguridad/usuario','UsuarioController');
Route::resource('layouts/app','HomeController');

Route::auth();

Route::get('/home', 'HomeController@index');
//Route::get('/{app}', 'HomeController@index');


